#include "common.h"
#include "bgfx_utils.h"
#include "imgui/imgui.h"

namespace
{

struct PosColorVertex
{
  float m_x;
  float m_y;
  float m_z;
  uint32_t m_abgr;

  static void init()
  {
    ms_decl
      .begin()
      .add(bgfx::Attrib::Position, 3, bgfx::AttribType::Float)
      .add(bgfx::Attrib::Color0,   4, bgfx::AttribType::Uint8, true)
      .end();
  };

  static bgfx::VertexDecl ms_decl;
};

bgfx::VertexDecl PosColorVertex::ms_decl;

struct PosUvVertex
{
  float m_x;
  float m_y;
  float m_z;
  float m_u;
  float m_v;

  static void init()
  {
    ms_decl
      .begin()
      .add(bgfx::Attrib::Position,  3, bgfx::AttribType::Float)
      .add(bgfx::Attrib::TexCoord0, 2, bgfx::AttribType::Float)
      .end();
  };

  static bgfx::VertexDecl ms_decl;
};

bgfx::VertexDecl PosUvVertex::ms_decl;

static PosColorVertex s_cubeVertices[] =
{
  {-1.0f,  1.0f,  1.0f, 0xff000000 },
  { 1.0f,  1.0f,  1.0f, 0xff0000ff },
  {-1.0f, -1.0f,  1.0f, 0xff00ff00 },
  { 1.0f, -1.0f,  1.0f, 0xff00ffff },
  {-1.0f,  1.0f, -1.0f, 0xffff0000 },
  { 1.0f,  1.0f, -1.0f, 0xffff00ff },
  {-1.0f, -1.0f, -1.0f, 0xffffff00 },
  { 1.0f, -1.0f, -1.0f, 0xffffffff },
};

static PosUvVertex s_cubeVerticesUv[] =
{
  {-1.0f,  1.0f,  1.0f, 0.0f, 1.0f },
  { 1.0f,  1.0f,  1.0f, 1.0f, 1.0f },
  {-1.0f, -1.0f,  1.0f, 0.0f, 0.0f },
  { 1.0f, -1.0f,  1.0f, 1.0f, 0.0f },
  {-1.0f,  1.0f, -1.0f, 0.0f, 1.0f },
  { 1.0f,  1.0f, -1.0f, 1.0f, 1.0f },
  {-1.0f, -1.0f, -1.0f, 0.0f, 0.0f },
  { 1.0f, -1.0f, -1.0f, 1.0f, 0.0f },
};

static const uint16_t s_cubeTriList[] =
{
  0, 1, 2, // 0
  1, 3, 2,
  4, 6, 5, // 2
  5, 6, 7,
  0, 2, 4, // 4
  4, 2, 6,
  1, 5, 3, // 6
  5, 7, 3,
  0, 4, 1, // 8
  4, 5, 1,
  2, 3, 6, // 10
  6, 3, 7,
};

class Wizy : public entry::AppI
{
public:
  Wizy(const char* name, const char* description)
    : entry::AppI(name, description)
    , m_pt(0)
  {
  }

  void init(int32_t argc, const char* const* argv, uint32_t width, uint32_t height) override
  {
    Args args(argc, argv);

    m_width  = width;
    m_height = height;
    m_debug  = BGFX_DEBUG_NONE;
    m_reset  = BGFX_RESET_VSYNC;

    // Initialise bgfx
    bgfx::Init init;
    init.type     = args.m_type;
    init.vendorId = args.m_pciId;
    init.resolution.width  = m_width;
    init.resolution.height = m_height;
    init.resolution.reset  = m_reset;
    bgfx::init(init);

    // Set debug level
    bgfx::setDebug(m_debug);

    // Set view 0 clear state
    bgfx::setViewClear(0
      , BGFX_CLEAR_COLOR|BGFX_CLEAR_DEPTH
      , 0x303030ff
      , 1.0f
      , 0
      );

    // Initialise vertex stream declaration
    PosColorVertex::init();
    PosUvVertex::init();

    // Create static vertex buffer.
    m_vbh = bgfx::createVertexBuffer(bgfx::makeRef(s_cubeVerticesUv, sizeof(s_cubeVerticesUv)), PosUvVertex::ms_decl);
    m_ibh = bgfx::createIndexBuffer(bgfx::makeRef(s_cubeTriList, sizeof(s_cubeTriList)));
    m_program = loadProgram("vs_wizy", "fs_wizy");

    m_treeTexture = loadTexture("resources/tree.png");

    m_samplerAlbedo = bgfx::createUniform("sampler_albedo", bgfx::UniformType::Int1);

    // Get initial time
    m_timeOffset = bx::getHPCounter();

    // Initialise imgui
    imguiCreate();
  }

  virtual int shutdown() override
  {
    imguiDestroy();

    // Clean up resources
    bgfx::destroy(m_ibh);
    bgfx::destroy(m_vbh);
    bgfx::destroy(m_program);

    bgfx::destroy(m_treeTexture);

    bgfx::destroy(m_samplerAlbedo);

    // Shut down bgfx
    bgfx::shutdown();

    return 0;
  }

  bool update() override
  {
    if (!entry::processEvents(m_width, m_height, m_debug, m_reset, &m_mouseState) )
    {
      imguiBeginFrame(m_mouseState.m_mx
        ,  m_mouseState.m_my
        , (m_mouseState.m_buttons[entry::MouseButton::Left  ] ? IMGUI_MBUT_LEFT   : 0)
        | (m_mouseState.m_buttons[entry::MouseButton::Right ] ? IMGUI_MBUT_RIGHT  : 0)
        | (m_mouseState.m_buttons[entry::MouseButton::Middle] ? IMGUI_MBUT_MIDDLE : 0)
        ,  m_mouseState.m_mz
        , uint16_t(m_width)
        , uint16_t(m_height)
        );

      showExampleDialog(this);

      ImGui::SetNextWindowPos(ImVec2(m_width - m_width / 5.0f - 10.0f, 10.0f), ImGuiCond_FirstUseEver);
      ImGui::SetNextWindowSize(ImVec2(m_width / 5.0f, m_height / 3.5f), ImGuiCond_FirstUseEver);
      ImGui::Begin("Settings", NULL, 0);

      ImGui::End();

      imguiEndFrame();

      float time = (float)( (bx::getHPCounter()-m_timeOffset)/double(bx::getHPFrequency() ) );

      float at[3]  = { 0.0f, 0.0f,   0.0f };
      float eye[3] = { 0.0f, 0.0f, -35.0f };

      // Set view and projection matrix
      {
        float view[16];
        bx::mtxLookAt(view, eye, at);

        float proj[16];
        bx::mtxProj(proj, 60.0f, float(m_width)/float(m_height), 0.1f, 100.0f, bgfx::getCaps()->homogeneousDepth);
        bgfx::setViewTransform(0, view, proj);

        // Set view 0 to default viewport
        bgfx::setViewRect(0, 0, 0, uint16_t(m_width), uint16_t(m_height) );
      }

      // Dummy draw call to make sure view 0 is cleared if no other draw calls are issued
      bgfx::touch(0);

      bgfx::IndexBufferHandle ibh = m_ibh;
      uint64_t state = 0
        | BGFX_STATE_WRITE_RGB
        | BGFX_STATE_WRITE_A
        | BGFX_STATE_WRITE_Z
        | BGFX_STATE_DEPTH_TEST_LESS
        | BGFX_STATE_CULL_CW
        | BGFX_STATE_MSAA
        ;

      // Submit 11x11 cubes.
      float mtx[16];
      float scale[16];
      bx::mtxRotateXY(mtx, time, time);
      bx::mtxScale(scale, 10.0f);
      bx::mtxMul(mtx, mtx, scale);
      mtx[12] = 0.0f;
      mtx[13] = 0.0f;
      mtx[14] = 0.0f;

      // Set model matrix for rendering.
      bgfx::setTransform(mtx);

      // Set vertex and index buffer.
      bgfx::setVertexBuffer(0, m_vbh);
      bgfx::setIndexBuffer(ibh);

      // Set render states.
      bgfx::setState(state);

      // Set texture
      bgfx::setTexture(0, m_samplerAlbedo, m_treeTexture);

      // Submit primitive for rendering to view 0.
      bgfx::submit(0, m_program);

      // Advance to next frame. Rendering thread will be kicked to
      // process submitted rendering primitives.
      bgfx::frame();

      return true;
    }

    return false;
  }

  entry::MouseState m_mouseState;

  uint32_t m_width;
  uint32_t m_height;
  uint32_t m_debug;
  uint32_t m_reset;
  bgfx::VertexBufferHandle m_vbh;
  bgfx::IndexBufferHandle m_ibh;
  bgfx::ProgramHandle m_program;
  bgfx::TextureHandle m_treeTexture;
  bgfx::UniformHandle m_samplerAlbedo;
  int64_t m_timeOffset;
  int32_t m_pt;
};

} // namespace

int _main_(int argc, char** argv)
{
    Wizy app("Wizy", "");
    return entry::runApp(&app, argc, argv);
}
